# USB HID Mouse Jitter using Raspberry Pico (RP2040)

## Introduction
This is a small saturday project for creating a **USB HID Mouse jigger**. Especially useful if you want to appear online and you are not able/allowed to use a software method to achieve that.

In addition it can be used as a reference on how to use the [TinyUSB](https://github.com/hathach/tinyusb) library with Raspberry Pico.
![Alt Text](media/demo_mouse_jigger.gif)

## Future work and support. 
This is just a Saturday project do not expect any support or future improvements. It should be a plug and play solution. Shall any issue arise I might look into that.  

## Installation
In the *Project Releases* as well as the [build](build/) directory,  you can find both the .elf file as well as the .uf2 fle format. Flash the program to the mcu according to the official [documentation](https://datasheets.raspberrypi.com/pico/getting-started-with-pico.pdf). 

*TLDR*: Drag and Drop the .uf2 file to the Pico when it is mounted as a mass media device. If you do not know how to do that then you should really check the documentation **#RTFM**. 


